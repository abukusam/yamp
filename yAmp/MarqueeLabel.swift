import Cocoa
import AVFoundation


class MarqueeLabel: NSButton, CAAnimationDelegate {
    
    let MASK_LEFT: Double = 0.05
    let MASK_RIGHT: Double = 0.95

    let textLayer: CATextLayer = CATextLayer()
    let maskLayer: CAGradientLayer = CAGradientLayer()
    var shouldScroll: Bool = false
    var scrollingTimer: Timer?
    
    let scrollInterval: TimeInterval = 0.5
    let pauseDuration: TimeInterval = 1.0
    let speed: Double = 50.0
    let fontSize: CGFloat = 16.0
    let tfont: NSFont = NSFont(name: "Arial", size: 16.0)!
    let foregroundColor: NSColor = NSColor.blue
    let backgroundColor: NSColor = NSColor.clear
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        
        self.textLayer.font = tfont
        self.textLayer.fontSize = fontSize
        self.textLayer.string = "yAmp"
        self.textLayer.alignmentMode = kCAAlignmentCenter
        self.textLayer.backgroundColor = foregroundColor.cgColor
        self.textLayer.frame = self.bounds
        
        self.maskLayer.anchorPoint = CGPoint(x: 0.0, y: 0.0)
        self.maskLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        self.maskLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        self.maskLayer.locations = [NSNumber(value: 0.0 as Double), NSNumber(value: MASK_LEFT as Double), NSNumber(value: MASK_RIGHT as Double), NSNumber(value: 1.0 as Double)]
        self.maskLayer.colors = [(NSColor.clear.cgColor), (NSColor.black.cgColor), (NSColor.black.cgColor), (NSColor.clear.cgColor)]
    }
    
    override func viewWillDraw() {
        self.maskLayer.frame = layer!.bounds
        layer!.mask = self.maskLayer
        layer!.addSublayer(self.textLayer)
    }
    
    func animate() {
        let to: CGPoint = CGPoint(x: self.textLayer.bounds.size.width/2 - fabs(layer!.bounds.size.width - self.textLayer.bounds.size.width), y: 12)
        //let to: CGPoint = CGPoint(x: layer!.bounds.size.width - self.textLayer.bounds.size.width - layer!.bounds.size.width * (1.0 - CGFloat(MASK_RIGHT)), y: self.textLayer.position.y)
        
        let animationLeft: CABasicAnimation = CABasicAnimation(keyPath: "position")
        animationLeft.beginTime = 0.0
        animationLeft.duration = fmax(fabs(Double(self.textLayer.position.x - to.x)) / self.speed, 1.0)
        //animationLeft.fromValue = NSValue(point: self.textLayer.position)
        animationLeft.fromValue = NSValue(point: CGPoint(x: self.textLayer.bounds.size.width/2, y: 12))
        animationLeft.toValue = NSValue(point: to)
        
        let animationPause: CABasicAnimation = CABasicAnimation(keyPath: "position")
        animationPause.beginTime = animationLeft.duration
        animationPause.duration = self.pauseDuration
        animationPause.fromValue = animationLeft.toValue
        animationPause.toValue = animationLeft.toValue
        
        let animationRight: CABasicAnimation = CABasicAnimation(keyPath: "position")
        animationRight.beginTime = animationLeft.duration + animationPause.duration
        animationRight.duration = animationLeft.duration
        animationRight.fromValue = animationLeft.toValue
        animationRight.toValue = animationLeft.fromValue
        
        let group: CAAnimationGroup = CAAnimationGroup()
        group.duration = animationRight.duration + animationPause.duration + animationLeft.duration
        group.animations = [animationRight, animationPause, animationLeft]
        group.delegate = self
        self.textLayer.add(group, forKey: "position")
    }
    
    func setText(_ aString: String) {
//        if (String(describing: self.textLayer.string) == aString) {
//            return
//        }
        self.textLayer.string = aString
        self.toolTip = aString
        
        let twidth = (aString as NSString).size(withAttributes: [NSFontAttributeName: tfont]).width + 6
        self.textLayer.bounds.size.width = twidth
        self.textLayer.position.x = twidth/2
        setvShouldScroll(self.textLayer.bounds.size.width > layer!.bounds.size.width)
    }
    
    open func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        if self.shouldScroll && flag {
            // Reset the scrolling timer
            self.setupScrollingTimer()
        }
    }
    
    func setvShouldScroll(_ vshouldScroll: Bool) {
        self.shouldScroll = vshouldScroll
        self.textLayer.removeAllAnimations()
        if self.shouldScroll {
            // Start the timer
            self.setupScrollingTimer()
        }
        else {
            // Invalidate the timer if it is set
            self.scrollingTimer?.invalidate()
        }
    }
    
    func setupScrollingTimer() {
        self.scrollingTimer = Timer.scheduledTimer(timeInterval: self.scrollInterval, target: self, selector: #selector(MarqueeLabel.animate), userInfo: nil, repeats: false)
    }
}

