import Foundation


class YCHelper {
    class func tagEnc(_ str: String) -> String {
        // 5 NSISOLatin1StringEncoding, 12 NSWindowsCP1252StringEncoding, 14 NSWindowsCP1254StringEncoding
        if let data = str.data(using: String.Encoding.isoLatin1) {
            if let ttitle = NSString(data: data, encoding: String.Encoding.windowsCP1251.rawValue) {
                return ttitle as String
            }
        }
        return ""
    }
    
    class func matchesInCapturingGroups(_ text: String, pattern: String) -> [String] {
        var results = [String]()
        
        let textRange = NSMakeRange(0, text.lengthOfBytes(using: String.Encoding.utf8))
        
        do {
            let regex = try NSRegularExpression(pattern: pattern, options: [])
            let matches = regex.matches(in: text, options: NSRegularExpression.MatchingOptions.reportCompletion, range: textRange)
            
            for index in 1..<matches[0].numberOfRanges {
                results.append((text as NSString).substring(with: matches[0].rangeAt(index)))
            }
            return results
        } catch {
            return []
        }
    }
    
    class func randRange(_ lower: Int , upper: Int) -> Int {
        return lower + Int(arc4random_uniform(UInt32(upper - lower + 1)))
    }
    
    class func fileExists(_ url: URL) -> Bool {
        return FileManager.default.fileExists(atPath: url.path)
    }
    
    class func displayTimeStringFromSeconds(_ seconds: Double) -> String {
        let totalSeconds: Int = Int(ceil(seconds));
        let secondsComponent: Int = totalSeconds % 60;
        let minutesComponent: Int = (totalSeconds / 60) % 60;
        return NSString(format:"%02d:%02d", minutesComponent, secondsComponent) as String
    }
}
