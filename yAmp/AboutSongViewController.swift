import Cocoa

class AboutSongViewController: NSViewController {
    @IBOutlet weak var filepathLabel: NSTextField!
    
    @IBOutlet weak var titleLabel: NSTextField!
    @IBOutlet weak var artistLabel: NSTextField!
    @IBOutlet weak var albumLabel: NSTextField!
    @IBOutlet weak var creatorLabel: NSTextField!
    @IBOutlet weak var typeLabel: NSTextField!
    @IBOutlet weak var subjectLabel: NSTextField!
    @IBOutlet weak var copyrightsLabel: NSTextField!
    @IBOutlet weak var languageLabel: NSTextField!
    
    @IBOutlet weak var artworkImage: NSImageView!
    
    var song: Song? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let song = song {
            filepathLabel.stringValue = song.url.path
            
            titleLabel.stringValue = song.data.title
            artistLabel.stringValue = song.data.artist
            albumLabel.stringValue = song.data.albumName
            creatorLabel.stringValue = song.data.artist
            typeLabel.stringValue = song.data.type
            subjectLabel.stringValue = song.data.subject
            copyrightsLabel.stringValue = song.data.copyrights
            languageLabel.stringValue = song.data.language
            
            artworkImage.image = song.data.artwork
        }
    }
    
    @IBAction func saveClick(_ sender: NSButton) {
        self.dismiss(nil)
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)!
    }
    
}
