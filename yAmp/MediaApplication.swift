import AppKit
import Cocoa

extension Bool {
    init<T : Integer>(_ integer: T){
        self.init(integer != 0)
    }
}

class MediaApplication: NSApplication {
    
    override func sendEvent(_ event: NSEvent) {
        if (event.type == .systemDefined && event.subtype.rawValue == 8) {
            let keyCode = ((event.data1 & 0xFFFF0000) >> 16)
            let keyFlags = (event.data1 & 0x0000FFFF)
            // Get the key state. 0xA is KeyDown, OxB is KeyUp
            let keyState = (((keyFlags & 0xFF00) >> 8)) == 0xA
            let keyRepeat = (keyFlags & 0x1)
            mediaKeyEvent(Int32(keyCode), state: keyState, keyRepeat: Bool(keyRepeat))
        }
        
        super.sendEvent(event)
    }
    
    func mediaKeyEvent(_ key: Int32, state: Bool, keyRepeat: Bool) {
        if (state) {
            switch(key) {
            case NX_KEYTYPE_PLAY:
                NotificationCenter.default.post(name: Notification.Name(rawValue: "pauseClick"), object: nil)
                break
            case NX_KEYTYPE_FAST:
                NotificationCenter.default.post(name: Notification.Name(rawValue: "nextClick"), object: nil)
                break
            case NX_KEYTYPE_REWIND:
                NotificationCenter.default.post(name: Notification.Name(rawValue: "prevClick"), object: nil)
                break
            default:
                break
            }
        }
    }
    
}
