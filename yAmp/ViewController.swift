import AVFoundation
import Cocoa

extension UserDefaults {
    func hasKey(_ key: String) -> Bool {
        return object(forKey: key) != nil
    }
}

open class ViewController: NSViewController, NSTableViewDataSource, NSTableViewDelegate, AVAudioPlayerDelegate {
    
    open class Preferences: NSObject {
        public enum Types: String {
            case bool = "Bool"
            case int = "Int"
            case float = "Float"
            case string = "String"
        }
        
        var isInit = true
        
        let defaults = UserDefaults.standard
        
        open var currentNumSong: Int = -1 {
            didSet {
                if !isInit {
                    defaults.set(currentNumSong, forKey: "currentNumSong")
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "playlistReloadData"), object: nil)
                }
            }
        }
        
        open var volume: Float = 1.0 {
            didSet {
                if !isInit {
                    defaults.set(volume, forKey: "volume")
                }
            }
        }
        
        open var shuffleEnabled: Bool = true {
            didSet {
                if !isInit {
                    defaults.set(shuffleEnabled, forKey: "shuffleEnabled")
                    NSLog("shuffleEnabled didSet: \(shuffleEnabled)")
                }
            }
        }
        
        open var showSongsNumbers: Bool = false {
            didSet {
                if !isInit {
                    defaults.set(showSongsNumbers, forKey: "showSongsNumbers")
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "playlistReloadData"), object: nil)
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "titleBarReload"), object: nil)
                }
            }
        }
        
        open var playlistFollowsCurrentSong: Bool = true {
            didSet {
                if !isInit {
                    defaults.set(playlistFollowsCurrentSong, forKey: "playlistFollowsCurrentSong")
                }
            }
        }
        
        open func load() {
            let m = Mirror(reflecting: self).children
            for mi in m {
                //defaults.removeObjectForKey(mi.label!)
                
                let dt = type(of: (mi.value))
                if dt != UserDefaults.self && defaults.hasKey(mi.label!) {
                    let stype = String(describing: dt)
                    
                    if let type = Types(rawValue: stype) {
                        switch type {
                        case .bool:
                            self.setValue(defaults.bool(forKey: mi.label!), forKey: mi.label!)
                        case .int:
                            self.setValue(defaults.integer(forKey: mi.label!), forKey: mi.label!)
                        case .float:
                            self.setValue(defaults.float(forKey: mi.label!), forKey: mi.label!)
                        default:
                            print("hui: \(mi.label!)")
                        }
                    }
                }
            }
        }
    }
    
    open static var preferences = Preferences()
    let orangeColor = NSColor(calibratedRed: 1, green: 210.0/256.0, blue: 99.0/256.0, alpha: 0.9)
    
    @IBOutlet weak open var playlistTableView: NSTableView!
    @IBOutlet weak var titleLable: MarqueeLabel!
    @IBOutlet weak var volumeSlider: NSSlider!
    @IBOutlet weak var timerLabel: NSTextField!
    @IBOutlet weak var timerSlider: NSSlider!
    @IBOutlet weak var shuffleCheckbox: NSButton!
    @IBOutlet weak var bitrateLable: NSTextField!
    
    let moc = (NSApplication.shared().delegate as! AppDelegate).managedObjectContext
    
    var player: AVAudioPlayer = AVAudioPlayer()
    var wasPlaying = false
    var currentFile: URL!
    var songs = [Song]() {
        didSet {
            if currentNumSong > songs.count - 1 {
                setNumSong(songs.count - 1, setCurrentSong: false)
            }
        }
    }
    var prevNums: [Int] = []
    var currentSong: Song? = nil {
        didSet {
            if let cs = currentSong {
                if !YCHelper.fileExists(cs.url) {
                    moc.delete(cs)
                    songs.remove(at: currentNumSong)
                    playlistTableView.reloadData()
                    currentNumSong += 1
                } else {
                    setTitleLabel(cs)
                    
                    let meta: MDItem = MDItemCreate(kCFAllocatorDefault, cs.url.path as CFString!)
                    let bitrate_num: NSNumber = MDItemCopyAttribute(meta, kMDItemAudioBitRate) as! NSNumber
                    let bitrate = Int(bitrate_num) / 1000
                    do {
                        let af = try AVAudioFile(forReading: cs.url as URL)
                        let sr = Int(af.fileFormat.sampleRate / 1000)
                        bitrateLable.stringValue = "\(af.fileFormat.channelCount) / \(sr) / \(bitrate)"
                    } catch let error as NSError {
                        print("Could not addToPlaylist \(error), \(error.userInfo)")
                    }
                    
                    if ViewController.preferences.playlistFollowsCurrentSong {
                        playlistTableView.scrollRowToVisible(currentNumSong)
                        //playlistTableView.selectRowIndexes(NSIndexSet(index: currentNumSong), byExtendingSelection: false)
                    }
                    
                    do {
                        player = try AVAudioPlayer(contentsOf: cs.url)
                        player.delegate = self
                        player.prepareToPlay()
                    } catch {
                        
                    }
                    
                    ViewController.preferences.currentNumSong = currentNumSong
                }
            }
        }
    }
    var currentNumSong: Int = -1 {
        didSet {
            if songs.count == 0 {
                return
            }
            if currentNumSong == -1 {
                currentNumSong = 0
            }
            if currentNumSong > songs.count - 1 {
                currentNumSong = 0
            }
        }
    }

    var volumeTimer: Timer!
    
    let playlistRowType = "playlistRowType"
    
    override open func viewWillAppear() {
        super.viewWillAppear()

        //titleLable.setShouldScroll(false)
        
        ViewController.preferences.load()
        ViewController.preferences.isInit = false
        NSLog("viewWillAppear: \(ViewController.preferences.currentNumSong)")
        setNumSong(ViewController.preferences.currentNumSong, setCurrentSong: true)
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        do {
            player = try AVAudioPlayer(contentsOf: NSURL.fileURL(withPath: Bundle.main.path(forResource: "yamp", ofType: "wav")!))
        } catch {
            
        }
        //player.delegate = self
        
//        for key in mainSettings.dictionaryRepresentation().keys {
//            mainSettings.removeObjectForKey(key)
//        }
        
        shuffleCheckbox.state = ViewController.preferences.shuffleEnabled ? 1 : 0
        NSLog("shuffleEnabled viewDidLoad: \(shuffleCheckbox.state) \(ViewController.preferences.shuffleEnabled)")
        volumeSlider.floatValue = ViewController.preferences.volume

//        for s in appDelegate.persistentStoreCoordinator.persistentStores {
//            print(s.URL?.path)
//            do {
//                try appDelegate.persistentStoreCoordinator.removePersistentStore(s)
//            } catch let error as NSError {
//                print("Could not removePersistentStore \(error), \(error.userInfo)")
//            }
//            
//        }
        
        fetch("")
        
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.nextClick(_:)), name: NSNotification.Name(rawValue: "nextClick"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.prevClick(_:)), name: NSNotification.Name(rawValue: "prevClick"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.pauseClick(_:)), name: NSNotification.Name(rawValue: "pauseClick"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.playlistReloadData(_:)), name: NSNotification.Name(rawValue: "playlistReloadData"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.titleBarReload(_:)), name: NSNotification.Name(rawValue: "titleBarReload"), object: nil)
        
        playlistTableView.register(forDraggedTypes: [playlistRowType, NSFilenamesPboardType])
        
        Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(ViewController.updateUI), userInfo: nil, repeats: true)
    }
    
    override open var representedObject: Any? {
        didSet {
            // Update the view, if already loaded.
        }
    }
    
    open func numberOfRows(in tableView: NSTableView) -> Int {
        return songs.count
    }
    
    open func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        var cellView: NSTableCellView? = nil
        let idCol = tableColumn!.identifier
        
        let song = songs[row]

        song.sorting = NSNumber(value: row)
        
        
        if idCol == "title" {
            cellView = tableView.make(withIdentifier: "titlecell", owner: self) as? NSTableCellView
            if ViewController.preferences.showSongsNumbers {
                let s = Int(song.sorting) + 1
                cellView!.textField!.stringValue = "\(s). \(song.title)"
            } else {
                cellView!.textField!.stringValue = song.title
            }
        } else {
            cellView = tableView.make(withIdentifier: "timecell", owner: self) as? NSTableCellView
            cellView!.textField!.stringValue = song.time
        }
        if song.uid == currentSong?.uid {
            setNumSong(row, setCurrentSong: false)
            cellView!.textField!.textColor = orangeColor
        } else {
            cellView!.textField!.textColor = NSColor.controlTextColor
        }
        return cellView
    }
    
    open func tableView(_ tableView: NSTableView, writeRowsWith writeRowsWithIndexes: IndexSet, to toPasteboard: NSPasteboard) -> Bool {
        
        let data = NSKeyedArchiver.archivedData(withRootObject: [writeRowsWithIndexes])
        toPasteboard.declareTypes([playlistRowType], owner: self)
        toPasteboard.setData(data, forType: playlistRowType)

        return true
    }
    
    open func tableView(_ tableView: NSTableView, validateDrop info: NSDraggingInfo, proposedRow row: Int, proposedDropOperation dropOperation: NSTableViewDropOperation) -> NSDragOperation {
        
        tableView.setDropRow(row, dropOperation: .above)
        return .move
    }
    
    open func tableView(_ tableView: NSTableView, acceptDrop info: NSDraggingInfo, row: Int, dropOperation: NSTableViewDropOperation) -> Bool {
        
        let pasteboard = info.draggingPasteboard()
        let rowData = pasteboard.data(forType: playlistRowType)
        
        if let propertyListForType = pasteboard.propertyList(forType: NSFilenamesPboardType) {
            let rowDataf = propertyListForType as! [String]
            
            for it in rowDataf.reversed() {
                let itUrl = URL(fileURLWithPath: it)
                addToPlaylist(itUrl, num: row, reload: false)
            }
            playlistTableView.reloadData()
            
            return true
        } else
        
        if rowData != nil {
            let dataArray = NSKeyedUnarchiver.unarchiveObject(with: rowData!) as! Array<IndexSet>
            let indexSet = dataArray[0]

            for i in indexSet {
                let item = songs[i]
                _moveItem(item, from: i, to: row)
            }
            playlistTableView.reloadData()
            return true
        }
        return false
    }
    
    func _moveItem(_ item: Song, from: Int, to: Int) {
        songs.remove(at: from)
        
        if to > songs.endIndex {
            songs.append(item)
        }
        else {
            songs.insert(item, at: to)
        }
    }
    
    open func tableViewSelectionDidChange(_ notification: Notification) {
        //let song = songs[self.playlistTableView.selectedRow] as! Song
        //titleLable.stringValue = song.title
    }
    
    @IBAction func doubleClickPlaylist(_ sender: NSTableView) {
        wasPlaying = true
        playSong(playlistTableView.selectedRow)
    }
    
    func setNumSong(_ num: Int, setCurrentSong: Bool) {
        currentNumSong = num
        if setCurrentSong && currentNumSong != -1 && songs.count > currentNumSong {
            currentSong = songs[currentNumSong]
        }
        //NSLog("setNumSong: \(ViewController.preferences.currentNumSong)")
        if currentNumSong != -1 && songs.count > currentNumSong {
            //player.volume = volumeSlider.floatValue
            
            timerSlider.doubleValue = 0

                timerSlider.maxValue = player.duration

            
            self.timerLabel.stringValue = "00:00"
        }
    }
    
    func playSong(_ num: Int) {
        //print("playSong \(num)")
        setNumSong(num, setCurrentSong: true)
        if wasPlaying {
            playerPlay()
        }
    }
    
    func showCurrentTitle() {
        if currentNumSong > -1 {
            setTitleLabel(currentSong!)
        }
    }
    
    func addToPlaylist(_ file: URL, sorting: NSNumber, num: Int?) {
        let ent = NSEntityDescription.entity(forEntityName: "Song", in: moc)
        let song = Song(filepath: file.absoluteString, sorting: sorting, entity: ent!, insertIntoManagedObjectContext: moc)
        
        do {
            try moc.save()
            if let toNum = num {
                songs.insert(song, at: toNum)
            } else {
                songs.append(song)
            }
        } catch let error as NSError {
            print("Could not addToPlaylist sorting \(error), \(error.userInfo)")
        }
    }
    
    func addToPlaylist(_ url: URL, num: Int?, reload: Bool) {
        do {
            var rsrc: AnyObject?
            try (url as NSURL).getResourceValue(&rsrc, forKey: URLResourceKey.isDirectoryKey)
            if let isDirectory = rsrc as? NSNumber {
                if isDirectory == true {
                    let fileManager = FileManager.default
                    let enumerator: FileManager.DirectoryEnumerator? = fileManager.enumerator(at: url, includingPropertiesForKeys: [URLResourceKey.localizedNameKey], options: FileManager.DirectoryEnumerationOptions(), errorHandler: nil)
                    var sort = songs.count
                    while let element = enumerator?.nextObject() as! URL? {
                        if element.pathExtension.lowercased() == "mp3" {
                            addToPlaylist(element, num: num, reload: reload)
                            sort = sort + 1
                        }
                    }
                    if reload {
                        playlistTableView.reloadData()
                    }
                } else {
                    if url.pathExtension.lowercased() == "mp3" {
                        addToPlaylist(url, sorting: NSNumber(value: num!), num: num)
                    }
                }
            }
        } catch let error as NSError {
            print("Could not addToPlaylist num \(error), \(error.userInfo)")
        }
        
        if reload {
            playlistTableView.reloadData()
        }
    }
    
    func setTitleLabel(_ song: Song) {
        if ViewController.preferences.showSongsNumbers {
            let n = currentNumSong + 1
            titleLable.setText("\(n). \(song.title) (\(song.time)) ***")
        } else {
            titleLable.setText("\(song.title) (\(song.time)) ***")
        }
    }
    
    func playlistReloadData(_ notification: Notification) {
        playlistTableView.reloadData()
    }
    
    func titleBarReload(_ notification: Notification) {
        showCurrentTitle()
    }
    
    func playNext() {
        var cNum = currentNumSong
        if shuffleCheckbox.state == NSOnState {
            cNum = YCHelper.randRange(0, upper: songs.count - 1)
            prevNums.append(cNum)
            if prevNums.count > 100 {
                prevNums.removeFirst()
            }
        } else {
            cNum += 1
            if cNum > songs.count - 1 {
                cNum = -1
            }
        }
        playSong(cNum)
    }
    
    public func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully
        flag: Bool) {
        self.nextClick(NSButton())
    }
    
    func playerPlay() {
        if currentNumSong != -1 {
            player.play()
            wasPlaying = true
        }
    }
    
    @IBAction func playClick(_ sender: NSButton) {
        if player.isPlaying {
            player.stop()
            playerPlay()
        } else {
            playerPlay()
        }
    }
    
    @IBAction func stopClick(_ sender: NSButton) {
        player.stop()
        wasPlaying = false
    }
    
    @IBAction func pauseClick(_ sender: NSButton) {
        if currentNumSong != -1 {
            if player.isPlaying {
                player.pause()
                wasPlaying = false
            } else {
                player.play()
                wasPlaying = true
            }
        }
    }
    
    @IBAction func nextClick(_ sender: NSButton) {
        playNext()
    }
    
    @IBAction func prevClick(_ sender: NSButton) {
        var cNum = currentNumSong
        if shuffleCheckbox.state == NSOnState {
            if prevNums.count > 1 {
                prevNums.removeLast()
                cNum = prevNums.last!
            } else {
                cNum = YCHelper.randRange(0, upper: songs.count - 1)
            }
        } else {
            cNum -= 1
            if cNum < 0 {
                cNum = songs.count - 1
            }
        }
        
        playSong(cNum)
    }
    
    func fetch(_ text: String) {
        let appDelegate = NSApplication.shared().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Song")
        let sortDescriptor = NSSortDescriptor(key: "sorting", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        if !text.isEmpty {
            let firstPredicate = NSPredicate(format: "title contains[cd] %@", text)
            fetchRequest.predicate = firstPredicate
        }
        
        do {
            let results = try managedContext.fetch(fetchRequest)
            songs = results as! [Song]
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        
        playlistTableView.reloadData()
    }
    
    @IBAction func searchWith(_ sender: NSSearchField) {
        fetch(sender.stringValue)
    }
    
    @IBAction func changeVolume(_ sender: NSSlider) {
        player.volume = sender.floatValue
        titleLable.setText(String(Int(sender.floatValue * 100)) + "%")
        ViewController.preferences.volume = sender.floatValue
        volumeTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(ViewController.showCurrentTitle), userInfo: nil, repeats: false)
    }
    
    @IBAction func changeDuration(_ sender: NSSlider) {
        if player.isPlaying {
            player.currentTime = sender.doubleValue
        }
    }
    
    @IBAction func shuffleChange(_ sender: NSButton) {
        ViewController.preferences.shuffleEnabled = Bool(sender.state)
        NSLog("shuffleEnabled shuffleChange: \(sender.state) \(ViewController.preferences.shuffleEnabled)")
    }
    
    @IBAction func copyPathClick(_ sender: NSMenuItem) {
        var urls: [String] = []
        for i in playlistTableView.selectedRowIndexes {
            let url = songs[i].url
            urls.append(url.path)
        }
        let list = urls.joined(separator: "\n")
        let pasteBoard = NSPasteboard.general()
        pasteBoard.clearContents()
        pasteBoard.setString(list, forType: NSStringPboardType)
    }
    
    @IBAction func showInFinderClick(_ sender: NSMenuItem) {
        var urls: [URL] = []
        for i in playlistTableView.selectedRowIndexes {
            urls.append(songs[i].url as URL)
        }
        NSWorkspace.shared().activateFileViewerSelecting(urls)
    }
    
    @IBAction func addToPlaylist(_ sender: AnyObject) {
        let myOpenDialog: NSOpenPanel = NSOpenPanel()
        myOpenDialog.canChooseDirectories = true
        myOpenDialog.runModal()
        
        let url = myOpenDialog.url

        myOpenDialog.close()
        addToPlaylist(url!, num: songs.count, reload: true)
    }
    
    @IBAction func clearPlaylistClick(_ sender: AnyObject) {
        let appDel = NSApplication.shared().delegate as! AppDelegate
        let context = appDel.managedObjectContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Song")
        do {
            let myList = try context.fetch(request)
            
            for bas: Any in myList {
                context.delete(bas as! NSManagedObject)
            }
            
            songs.removeAll()
            
            do {
                try context.save()
            } catch let error as NSError {
                print("Could not save \(error), \(error.userInfo)")
            }
            playlistTableView.reloadData()
            
        } catch let error as NSError {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    @IBAction func removeFromPlaylistClick(_ sender: NSMenuItem) {
        let appDel = NSApplication.shared().delegate as! AppDelegate
        let context = appDel.managedObjectContext

        for i in playlistTableView.selectedRowIndexes {
            context.delete(songs[i])
            songs.remove(at: i)
        }
        
        do {
            try context.save()
        } catch let error as NSError {
            print("Could not save \(error), \(error.userInfo)")
        }
        
        playlistTableView.reloadData()
    }
    
    @IBAction func removeFromPlaylistWithFileClick(_ sender: NSMenuItem) {
        let appDel = NSApplication.shared().delegate as! AppDelegate
        let context = appDel.managedObjectContext
        
        for i in playlistTableView.selectedRowIndexes {
            do {
                try FileManager.default.removeItem(at: songs[i].url as URL)
            } catch let error as NSError {
                print("Could not delete \(error), \(error.userInfo)")
            }
            
            context.delete(songs[i])
            songs.remove(at: i)
        }
        
        do {
            try context.save()
        } catch let error as NSError {
            print("Could not save \(error), \(error.userInfo)")
        }
        
        playlistTableView.reloadData()
    }
    
    @IBAction func refreshClick(_ sender: NSMenuItem) {
        for (i, song) in songs.enumerated() {
            if !YCHelper.fileExists(song.url) {
                moc.delete(song)
                songs.remove(at: i)
            }
        }
        playlistTableView.reloadData()
    }
    
    override open func prepare(for segue: NSStoryboardSegue, sender: Any?) {
        if segue.identifier == "aboutSegue" {
            if let aboutVC = segue.destinationController as? AboutSongViewController {
                aboutVC.song = currentSong
            }
        }
    }
    
    func updateUI() {
        if player.isPlaying && !self.timerSlider.isHighlighted {
            self.timerLabel.stringValue = YCHelper.displayTimeStringFromSeconds(player.currentTime)
            self.timerSlider.doubleValue = player.currentTime
        }
    }

}

