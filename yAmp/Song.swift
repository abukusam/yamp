import AVFoundation
import Cocoa

@objc(Song)
class Song: NSManagedObject {
    
    struct Data {
        let url: URL
        var title: String = ""
        var artist: String = ""
        var type: String = ""
        var albumName: String = ""
        var creator: String = ""
        var subject: String = ""
        var copyrights: String = ""
        var language: String = ""
        var publisher: String = ""
        
        var artwork: NSImage? = nil
        
        var fullTitle: String {
    
            var filename = ""
            if artist != "" && title != "" {
                return "\(artist) - \(title)"
            } else if title != "" {
                return title
            }
            
            filename = (url.deletingPathExtension().lastPathComponent)
//            var locName: AnyObject?
//            do {
//                try url.getResourceValue(&locName, forKey: NSURLLocalizedNameKey)
//                filename = locName as! String
//                filename = filename.substringToIndex(filename.endIndex.advancedBy(-4))
//            } catch let error as NSError {
//                print("Could not NSURLLocalizedNameKey \(error), \(error.userInfo)")
//            }
            
//            NSLog("\(filename)")
//            
//            let ma = YCHelper.matchesInCapturingGroups(filename, pattern: "^\\d*[\\._]?(.*)")
//            NSLog("\(ma)")
//            if ma.count > 0 {
//                filename = ma[0]
//            }
            
            filename = filename.trimmingCharacters(in: CharacterSet.whitespaces)
            return filename
        }
        
        init(url: URL) {
            self.url = url
            
            let playerItem = AVPlayerItem(url: url)
            
            let metadataList = playerItem.asset.commonMetadata
            
            for item in metadataList {
                switch item.commonKey! {
                    case "title":
                        title = YCHelper.tagEnc(item.stringValue!)
                    case "artist":
                        artist = YCHelper.tagEnc(item.stringValue!)
                    case "type":
                        type = YCHelper.tagEnc(item.stringValue!)
                    case "albumName":
                        albumName = YCHelper.tagEnc(item.stringValue!)
                    case "creator":
                        creator = YCHelper.tagEnc(item.stringValue!)
                    case "subject":
                        subject = YCHelper.tagEnc(item.stringValue!)
                    case "copyrights":
                        copyrights = YCHelper.tagEnc(item.stringValue!)
                    case "language":
                        language = YCHelper.tagEnc(item.stringValue!)
                    case "artwork":
                        artwork = NSImage(data: item.dataValue!)
                    case "publisher":
                        publisher = YCHelper.tagEnc(item.stringValue!)
                    
                    case "format":
                        break
                    case "contributor":
                        NSLog("contributor "+item.stringValue!)
                    case "creationDate":
                        break
                    case "identifier":
                        if let identifier = item.stringValue {
                            NSLog("identifier "+identifier)
                        }

                    default:
                        print(item.commonKey!)
                    }
            }
        }
    }
    
    @NSManaged var uid: String
    @NSManaged var title: String
    @NSManaged var titled: Bool
    @NSManaged var time: String
    @NSManaged var filepath: String
    @NSManaged var sorting: NSNumber
    
    convenience init(filepath: String, sorting: NSNumber, entity: NSEntityDescription, insertIntoManagedObjectContext context: NSManagedObjectContext?) {
        self.init(entity: entity, insertInto: context)
        self.uid = UUID().uuidString
        self.filepath = filepath
        self.sorting = sorting
        self.time = getTime()
        self.title = data.fullTitle != "" ? data.fullTitle : filepath
    }
    
    lazy var url: URL = {
        return URL(string: self.filepath)
    }()!
    
    lazy var data: Data = {
        return Data(url: self.url)
    }()
    
    func getTime() -> String {
        let asset = AVURLAsset(url: self.url, options: nil)
        let audioDuration = asset.duration
        return stringFromTimeInterval(CMTimeGetSeconds(audioDuration))
    }
    
    func stringFromTimeInterval(_ interval: TimeInterval) -> String {
        let interval = Int(interval)
        let seconds = interval % 60
        let minutes = (interval / 60) % 60
        let minutes_from_hours = (interval / 3600) * 60
        return String(format: "%02d:%02d", minutes + minutes_from_hours, seconds)
    }
    
}
