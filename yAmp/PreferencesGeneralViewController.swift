import Cocoa

class PreferencesGeneralViewController: NSViewController {
    @IBOutlet weak var showSongsNumbers: NSButton!
    @IBOutlet weak var playlistFollowsCurrentSong: NSButton!

    override func viewDidAppear() {
        super.viewDidAppear()
        showSongsNumbers.state = ViewController.preferences.showSongsNumbers ? 1 : 0
        playlistFollowsCurrentSong.state = ViewController.preferences.playlistFollowsCurrentSong ? 1 : 0
    }
    
    @IBAction func playlistShowNumberOfSongsClick(_ sender: NSButton) {
        ViewController.preferences.showSongsNumbers = sender.state == 1 ? true : false as Bool
    }
    
    @IBAction func playlistFollowsCurrentSongClick(_ sender: NSButton) {
        ViewController.preferences.playlistFollowsCurrentSong = sender.state == 1 ? true : false as Bool
    }
}
