#ifndef ObjCHelper_h
#define ObjCHelper_h


#endif /* ObjCHelper_h */


#import <Cocoa/Cocoa.h>
#import <Foundation/Foundation.h>
#import <QuartzCore/CATextLayer.h>

@interface ObjCHelper : NSControl

+(void)layoutSublayersOfLayer:(CALayer *)baselayer textlayer:(CATextLayer*)textlayer ml:(float)ml mr:(float)mr;

@end
