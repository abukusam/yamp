#import "ObjCHelper.h"

#import <QuartzCore/QuartzCore.h>
#import <QuartzCore/CALayer.h>
#import <QuartzCore/CATextLayer.h>

@interface ObjCHelper ()

+(void)layoutSublayersOfLayer:(CALayer *)baselayer textlayer:(CATextLayer*)textlayer ml:(float)ml mr:(float)mr;

@end

@implementation ObjCHelper

+(void)layoutSublayersOfLayer:(CALayer *)baselayer textlayer:(CATextLayer*)textlayer ml:(float)ml mr:(float)mr
{
    printf("%s", "layoutSublayersOfLayer");
    // If the Text Layer has become larger than the base layer, left-justify it.
    if (textlayer.bounds.size.width > baselayer.bounds.size.width*(mr-ml))
    {
        CAConstraint *horizontalConstraint = [CAConstraint constraintWithAttribute:kCAConstraintMinX relativeTo:@"superlayer" attribute:kCAConstraintMinX scale:1.f offset:baselayer.bounds.size.width*ml];
        CAConstraint *verticalConstraint = [CAConstraint constraintWithAttribute:kCAConstraintMidY relativeTo:@"superlayer" attribute:kCAConstraintMidY];
        [textlayer setConstraints:[NSArray arrayWithObjects:verticalConstraint, horizontalConstraint, nil]];
        
    } else {
        CAConstraint *horizontalConstraint = [CAConstraint constraintWithAttribute:kCAConstraintMidX relativeTo:@"superlayer" attribute:kCAConstraintMidX];
        CAConstraint *verticalConstraint = [CAConstraint constraintWithAttribute:kCAConstraintMidY relativeTo:@"superlayer" attribute:kCAConstraintMidY];
        [textlayer setConstraints:[NSArray arrayWithObjects:verticalConstraint, horizontalConstraint, nil]];
        
    }
}

@end
